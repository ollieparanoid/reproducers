#!/bin/sh -ex

work="$(pmbootstrap -q config work)"
temp="$work/cache_git/pmaports/temp/apk-replaces"

# Remove chroot, binary packages, test aport
pmbootstrap -q -y zap -p
rm -rf "$temp"

# Build test-base 1-r0 and test-override 1-r0
install -Dm644 APKBUILD.1 "$temp/test-base/APKBUILD"
install -Dm644 APKBUILD.2 "$temp/test-override/APKBUILD"
pmbootstrap -q build "test-base"
pmbootstrap -q build "test-override"

# Install test-override
pmbootstrap -q -y zap
pmbootstrap -q chroot -- apk add "test-override"

# Who provides testfile?
pmbootstrap -q chroot -- cat /etc/testfile

# Build test-base 2-r0
install -Dm644 APKBUILD.3 "$temp/test-base/APKBUILD"
pmbootstrap -q build "test-base"

# Upgrade
pmbootstrap -q chroot -- apk upgrade

# Who provides testfile?
pmbootstrap -q chroot -- cat /etc/testfile

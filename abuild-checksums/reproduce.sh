#!/bin/sh -ex

work="$(pmbootstrap -q config work)"
pkgname="checksums-test"
aport="$work/cache_git/pmaports/temp/$pkgname"

# Remove chroot, binary packages, test aport
pmbootstrap -q -y zap -p
rm -rf "$aport"

# Build v1 (has subpkg with install_if="$pkgname alpine-base")
mkdir -p "$aport"
cp -r APKBUILD dir1 dir2 "$aport"
pmbootstrap -q checksum "$pkgname"
cat "$aport/APKBUILD"

pmbootstrap -q build "$pkgname"

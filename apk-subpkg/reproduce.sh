#!/bin/sh -ex

work="$(pmbootstrap -q config work)"
pkgname="subpkg-test"
aport="$work/cache_git/pmaports/temp/$pkgname"

# Remove chroot, binary packages, test aport
pmbootstrap -q -y zap -p
rm -rf "$aport"

# Build v1 (has subpkg with install_if="$pkgname alpine-base")
install -Dm644 APKBUILD.v1 "$aport/APKBUILD"
pmbootstrap -q build "$pkgname"

# Install $pkgname explicitly and subpkg implicitly
pmbootstrap -q chroot -- apk add "$pkgname"

# Build v2 (has no subpkg)
install -Dm644 APKBUILD.v2 "$aport/APKBUILD"
pmbootstrap -q build "$pkgname"

# Remove old subpackage and update index
sudo rm "$work"/packages/*/*/"$pkgname"-subpkg-*.apk
pmbootstrap -q index

# Upgrade
pmbootstrap -q chroot -- apk upgrade

# List installed packages
pmbootstrap -q chroot -- apk info -vv | grep "$pkgname"

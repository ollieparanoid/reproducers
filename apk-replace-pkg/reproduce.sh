#!/bin/sh -ex

work="$(pmbootstrap -q config work)"
tempdir="$work/cache_git/pmaports/temp/replace-pkg"

# Remove chroot, binary packages, test aport
pmbootstrap -q -y zap -p
rm -rf "$tempdir"

pkgname="replace-pkg-test"
install -Dm644 APKBUILD.v1 "$tempdir/$pkgname/APKBUILD"
pmbootstrap -q build "$pkgname"

pmbootstrap -q chroot -- apk add "$pkgname"

pkgname="replace-pkg-test-new"
install -Dm644 APKBUILD.v2 "$tempdir/$pkgname/APKBUILD"
pmbootstrap -q build "$pkgname"

# Remove old package
sudo rm "$work"/packages/*/*/replace-pkg-test-1-r0.apk
pmbootstrap -q index

# Upgrade
pmbootstrap -q chroot -- apk upgrade

# List installed packages
pmbootstrap -q chroot -- apk info -vv | grep "replace-pkg-test"
